package main

import (
	"syscall/js"
)

func main() {
	message := "👋 Hello From GoLang 😃"

	document := js.Global().Get("document")
	h1 := document.Call("createElement", "h1")
	h1.Set("innerHTML", message)
	document.Get("body").Call("appendChild", h1)

	// Prevent the function from returning, 
	// which is required in a wasm module
	select {}
}
