## Initialize

```bash
go mod init gitlab.com/k33g_org/golang/1st-steps/05-testing
touch main.go
touch index.html
cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" .
```

## Build

```bash
GOOS=js GOARCH=wasm go build -o main.wasm
```

## Serve

```bash
python3 -m http.server
```

## Testing

```bash
GOOS=js GOARCH=wasm go test -exec="$(go env GOROOT)/misc/wasm/go_js_wasm_exec"
```