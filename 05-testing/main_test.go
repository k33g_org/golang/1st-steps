package main

import (
	"syscall/js"
	"testing"
)

func TestHello(t *testing.T) {
	// Hello function "wants" want (js.Value, []js.Value)
	this := js.ValueOf(nil)
	// first argument
	arg0 := js.ValueOf(map[string]interface{}{
		"firstName":"Bob",
		"lastName":"Morane", 
		}) 
	// array of arguments
	args := []js.Value{ arg0 }
	// call the function
	result := Hello(this, args).(map[string]interface{})
	// test the result
	if result["message"] != "👋 Hello Bob Morane" {
		t.Error("Incorrect result, expected '👋 Hello Bob Morane', got", result["message"])
	}
}
