## Initialize

```bash
go mod init gitlab.com/k33g_org/golang/1st-steps/06-json-function
touch main.go
touch index.html
wget https://raw.githubusercontent.com/tinygo-org/tinygo/release/targets/wasm_exec.js
```

## Build

```bash
tinygo build -o main.wasm -target wasm ./main.go
ls -lh *.wasm
```

## Serve

```bash
python3 -m http.server
```