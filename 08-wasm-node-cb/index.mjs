import fs from "fs"
import * as _ from "./wasm_exec.js"

function sayHello(param1,param2, callback) {
  console.log("👋 from NodeJS", param1,param2)
  callback(param1,param2)
}
global.sayHello = sayHello

function runWasm(wasmFile) {
  const go = new Go()
  // 🖐 workaround
  go.importObject.env["syscall/js.finalizeRef"] = () => {}

  return new Promise((resolve, reject) => {
    WebAssembly.instantiate(wasmFile, go.importObject)
    .then(result => {
      go.run(result.instance) 
      resolve(result.instance)
    })
    .catch(error => {
      reject(error)
    })
  })
}

const wasmFile = fs.readFileSync('./main.wasm')

runWasm(wasmFile).then(wasm => {
  // foo
})
