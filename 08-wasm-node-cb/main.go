package main

import (
	"syscall/js"
)

func main() {

	callback := func(this js.Value, args []js.Value)  interface{} {
		println(args[0].String())
		println(args[1].String())
		return ""
	}
	
	js.Global().Call("sayHello", "🐳 one", "🌺 two", js.FuncOf(callback))
	
	<-make(chan bool)

}
