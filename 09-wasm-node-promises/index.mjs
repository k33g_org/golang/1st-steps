import fs from "fs"
import * as _ from "./wasm_exec.js"

function sayHello(yes, no) {
  return new Promise((resolve, reject) => {

    setTimeout(_ => {
      if (yes>no) { resolve("🙂") } else reject("😡")
    }, 3000)

  })
}

global.sayHello = sayHello


function runWasm(wasmFile) {
  const go = new Go()
  // 🖐 workaround
  go.importObject.env["syscall/js.finalizeRef"] = () => {}

  return new Promise((resolve, reject) => {
    WebAssembly.instantiate(wasmFile, go.importObject)
    .then(result => {
      go.run(result.instance) 
      resolve(result.instance)
    })
    .catch(error => {
      reject(error)
    })
  })
}

const wasmFile = fs.readFileSync('./main.wasm')

runWasm(wasmFile).then(wasm => {
  // foo
})
