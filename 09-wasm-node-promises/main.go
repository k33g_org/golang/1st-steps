package main

import (
	"syscall/js"
)

func main() {

	
	thenFunc := func(this js.Value, args []js.Value)  interface{} {
		println("All good:")
		println(args[0].String())
		return ""
	}

	catchFunc := func(this js.Value, args []js.Value)  interface{} {
		println("Ouch:")
		println(args[0].String())
		return ""
	}
	
  js.Global().Call("sayHello", 42, 24).Call("then", js.FuncOf(thenFunc)).Call("catch", js.FuncOf(catchFunc))
	
	js.Global().Call("sayHello", 24, 42).Call("then", js.FuncOf(thenFunc)).Call("catch", js.FuncOf(catchFunc))
		
	<-make(chan bool)

}
